<?php

/**
 * @file
 * Field documentation reports.
 */

/**
 * Generates table of field data for given content type, or all.
 *
 * @param string $content_type
 *   Optional machine name of content type to be described.
 *
 * @return array
 *   Report content as renderable array.
 */
function form_documenter_document_type($content_type = NULL) {

  // Retrieve information about nodes above and beyond entity_info.
  $node_types = node_type_get_types();

  // If content type(s) supplied, parse and validate.
  if ($content_type) {
    $content_types = explode(' ', $content_type);
    $odd_types = array_diff($content_types, array_keys($node_types));
    if (!empty($odd_types)) {
      return [
        '#markup' => t('Content type(s) @types do(es) not exist',
          ['@types' => implode(', ', $odd_types)]),
        '#prefix' => '<pre>',
        '#suffix' => '</pre>',
      ];
    }
    $content_types_names = [];
    foreach ($content_types as $bundle) {
      $content_types_names[] = $node_types[$bundle]->name;
    };
    $page_title = implode(', ', $content_types_names);
  }
  else {
    $content_types = array_keys($node_types);
    $page_title = t('all content types');
  }

  // Gather information about fields and widgets from system.
  $field_types = field_info_field_types();
  // Make field descriptions less verbose and remove period at end.
  foreach (array_keys($field_types) as $field_type) {
    $field_types[$field_type]['description'] = substr(str_replace(['This field stores',' in the database'], ['Stores', ''], $field_types[$field_type]['description']), 0, -1);
  }
  $widget_types = field_info_widget_types();


  // Lists of information we want from field base and instance settings.
  $field_settings = ['field_name', 'type', 'cardinality'];
  $instance_settings = ['label'];

  // Basic row template.
  $data_template = [
    'group' => '',
    'label' => '',
    'field_name' => '',
    'type' => '',
    'widget' => '',
    'cardinality' => '',
    'description' => '',
    'required' => '',
    'options' => '',
  ];

  // Special purpose title template.
  $title_template = [
    'group' => '',
    'label' => 'Title',
    'field_name' => 'title',
    'type' => 'Title<br/>A string of no more than 255 chars',
    'widget' => 'Text field',
    'cardinality' => '1',
    'description' => '',
    'required' => 1,
    'options' => '',
  ];

  $column_headings = [
    'Grouping',
    'Prompt',
    'Field',
    'Data',
    'Input Widget',
    '# allowed (-1 = unlimited)',
    'Help text',
    'Required (1 = Yes)',
    'Allowed Options',
  ];

  // Make sure that node form code is present.
  module_load_include('inc', 'node', 'node.pages');

  // Initialize output variable.
  $out = [];
  foreach ($content_types as $bundle) {

    // The node add form for the current user.
    $node_form = form_documenter_get_node_form($bundle);
    $field_list = form_documenter_get_field_list($node_form);

    // Get the instance info for this content type.
    $instances = field_info_instances('node', $bundle);

    // Now construct $rows of information about the fields found in the form.
    $rows = [];
    $related_entities = [];
    foreach ($field_list as $field_name) {
      // Titles are not really fields, and have to be handled separately.
      if ('title' == $field_name) {
        $rows['title'] = array_merge($title_template, ['label' => $node_types[$bundle]->title_label]);
        continue;
      }

      $row = $data_template;

      // Base field information.
      $field_info = field_info_field($field_name);
      if ($field_info) {
        $field_type = $field_types[$field_info['type']];
        foreach ($field_settings as $field_setting) {
          if ( 'type' == $field_setting ) {
            $row['type'] = $field_type['label'] . '<br/>' . $field_type['description'];
          } else {
            $row[$field_setting] = $field_info[$field_setting];
          }
        }
        // Fields needing special processing for more information:
        switch ($field_info['type']) {
          case 'number_decimal':
            if (isset($field_info['settings']['precision'])) {
              $scale = $field_info['settings']['scale'];
              $max_size = $field_info['settings']['precision'] - $scale;
              $exemplar = 10**($max_size) - 10**(-$scale);
              $row['type'] .= ' ' . t('up to @exemplar', ['@exemplar'=>number_format($exemplar, $scale)]);
            }
            break;
          case 'entityreference':
            if (isset($field_info['settings']['handler_settings']['target_bundles'])) {
              $row['type'] .= ': ' . implode(', ', $field_info['settings']['handler_settings']['target_bundles']);
              $related_entities += $field_info['settings']['handler_settings']['target_bundles'];
            }
            else {
              $row['type'] .= ': ' . $field_info['settings']['target_type'];
              if (isset($field_info['settings']['handler_settings']['view'])) {
                $row['type'] .= ' ' . t('provided by @disp display of @view view', [
                  '@disp' => $field_info['settings']['handler_settings']['view']['display_name'],
                  '@view' => $field_info['settings']['handler_settings']['view']['view_name'],
                ]);
              }
            }
            break;
          case 'taxonomy_term_reference':
            if (isset($field_info['settings']['allowed_values']) && is_array($field_info['settings']['allowed_values'])) {
              foreach ($field_info['settings']['allowed_values'] as $allowed_value) {
                $row['type'] .= ' ' . t('from the @vocab taxonomy', ['@vocab' => $allowed_value['vocabulary']]);
              }
            }
            break;
          case 'text':
            if (isset($field_info['settings']['max_length'])) {
              $row['type'] .= ' ' . t('of no more than @max characters', ['@max' => $field_info['settings']['max_length']]);
            }
            break;
          case 'datetime':
            // Get granularity data.
            break;
        }

      }
      // Field not in database.
      else {
        $row['field_name'] = $field_name;
        $row['type'] = 'UNKNOWN';
      }

      // Field instance information.
      if (isset($instances[$field_name])) {
        $instance_info = $instances[$field_name];
        $row['widget'] = $widget_types[$instance_info['widget']['type']]['label'];
        foreach ($instance_settings as $instance_setting) {
          $row[$instance_setting] = $instance_info[$instance_setting];
        }
      }
      // Field not in database.
      else {
        $row['label'] = 'NO FIELD SETTINGS IN DB';
      }

      // Form information.
      if (isset($node_form['#group_children'][$field_name])) {
        $row['group'] = $node_form['#group_children'][$field_name];
      }
      $form_data =& $node_form[$field_name][LANGUAGE_NONE];
      $row['required'] = ((!empty($node_form[$field_name]['#required']) || !empty($form_data['#required'])) ? 1 : '');
      if (isset($form_data['#title'])) {
        $row['label'] = $form_data['#title'];
      }
      $row['description'] = isset($form_data['#description']) ? $form_data['#description'] : '';
      if (!empty($form_data['#options'])) {
        $i = 0;
        $options_array = [];
        foreach ($form_data['#options'] as $key => $value) {
          $options_array[] .= t('@key|@value', [
            '@key' => trim($key),
            '@value' => trim($value),
          ]);
          if ($i++ > 20) {
            break;
          }
        }
        $diff = count($form_data['#options']) - count($options_array);
        if ($diff) {
          $options_array[] = ' ...' . t('plus @ct more options', ['@ct' => $diff]);
        }
        $row['options'] = implode("<br/>\n", $options_array);
      }
      else {
        $row['options'] = '';
      }

      $rows[$field_name] = $row;
    }

    $table_array = form_documenter_table_array(TRUE);
    $table_array['#header'] = $column_headings;
    $table_array['#rows'] = $rows;
    $table_array['#sticky'] = TRUE;

    $render_array = [
      'field_table_header' => [
        '#markup' => t('@content_label (@content_type) form fields', [
          '@content_label' => $node_types[$bundle]->name,
          '@content_type' => $bundle,
        ]
        ),
        '#prefix' => '<h2>',
        '#suffix' => '</h2>',
      ],
    ];
    if (!empty($node_types[$bundle]->description)) {
      $render_array['content_description'] = [
        '#markup' => filter_xss($node_types[$bundle]->description),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ];
    }
    if (!empty($node_types[$bundle]->help)) {
      $render_array['form_help'] = [
        '#markup' => filter_xss($node_types[$bundle]->help),
        '#prefix' => '<p><strong>Form instructions</strong>:<br/>',
        '#suffix' => '</p>',
      ];
    }
    $render_array['field_table'] = $table_array;
    if (!empty($related_entities)) {
      $router_item = menu_get_item();
      foreach ($related_entities as &$entity) {
        $entity = l($entity, $router_item['path'] . '/' . $entity);
      }
      $render_array['related_header'] = [
        '#markup' => 'Related Entities',
        '#prefix' => '<h2>',
        '#suffix' => '</h2>',
      ];
      $render_array['related'] = [
        '#theme' => 'item_list',
        '#title' => '',
        '#type' => 'ul',
        '#attributes' => [],
        '#items' => $related_entities,
      ];
    }
    $out[] = $render_array;
  }

  // Fix the page title, which is altered by getting the node form.
  drupal_set_title(t('Field documentation for @page_title', ['@page_title' => $page_title]));
  return $out;
}

/**
 * Retrieves node form.
 *
 * @param string $bundle
 *   Machine name of content type.
 * @param int $uid
 *   Optional: uid of user for whom form is prepared (i.e. set of privileges).
 *
 * @return array|mixed
 *   The form array.
 */
function form_documenter_get_node_form($bundle, $uid = NULL) {
  if (!$uid) {
    $uid = $GLOBALS['user']->uid;
  }
  // Construct dummy node object.
  $node = (object) [
    'uid' => $uid,
    'name' => '',
    'type' => $bundle,
    'language' => LANGUAGE_NONE,
  ];
  return drupal_get_form($bundle . '_node_form', $node);
}

/**
 * Retrieves the list of fields from the node add form.
 *
 * @param array $node_form
 *    A node form to parse for fields.
 *
 * @return array
 *    Array of field machine names.
 */
function form_documenter_get_field_list(array $node_form) {

  // Process the form to the point where we can get field ordering out of it.
  $elements = $node_form;
  // This code from drupal_render().
  if (isset($node_form['#pre_render'])) {
    foreach ($node_form['#pre_render'] as $function) {
      if (function_exists($function)) {
        $elements = $function($elements);
      }
    }
  }

  // Recursively extract fields from the processed form.
  $field_list = [];
  form_documenter_get_fields($field_list, $elements, $elements);
  // Get add'l fields in field groups that may be hidden by ajax.
  if (!empty($node_form['#groups'])) {
    foreach ($node_form['#groups'] as $group) {
      if ('hidden' == $group->format_type) {
        $field_list = array_merge($field_list, $group->children);
      }
    }
  }

  return $field_list;
}

/**
 * Recursively generate list of fields as they appear in a nested form.
 *
 * @param array &$field_list
 *    The list of fields being built.
 * @param array &$element
 *    The form or parent element.
 * @param array &$form
 *    The parent form.
 */
function form_documenter_get_fields(array &$field_list, array &$element, array &$form) {
  // Get sorted list of children.
  // Could use element_get_visible_children, but that does not force a sort.
  foreach (element_children($element, TRUE) as $child) {
    if (isset($element[$child]['#access']) && !$element[$child]['#access']) {
      continue;
    }
    // Skip value and hidden elements, since they are not rendered.
    if (isset($element[$child]['#type']) && in_array($element[$child]['#type'], ['value', 'hidden'])) {
      continue;
    }
    // Detect what kind of thing the child is.
    $prefix = substr($child, 0, 6);
    // If a group, ignore and get its children.
    if ('group_' == $prefix && !empty($form['#groups'][$child]->children)) {
      form_documenter_get_fields($field_list, $element[$child], $form);
    }
    // If a field, title or body, grab it.
    elseif ('field_' == $prefix || in_array($child, ['title', 'body'])) {
      $field_list[] = $child;
    }
  }
}

/**
 * Utility function: provides array that theme_table() wants.
 *
 * Prevents notices from being generated.
 * keys: header, rows, attributes, caption, colgroups, sticky, empty.
 *
 * @param bool $as_render_array
 *    Whether to return renderable array with '#' properties
 *    (default false = array to pass directly to theme('table')).
 *
 * @return array
 *    Array with keys and default values that theme_table wants.
 */
function form_documenter_table_array($as_render_array = FALSE) {
  if ($as_render_array) {
    return [
      '#theme' => 'table',
      '#header' => [],
      '#rows' => [],
      '#attributes' => [],
      '#caption' => NULL,
      '#colgroups' => [],
      '#sticky' => NULL,
      '#empty' => NULL,
    ];
  }
  return [
    'header' => [],
    'rows' => [],
    'attributes' => [],
    'caption' => NULL,
    'colgroups' => [],
    'sticky' => NULL,
    'empty' => NULL,
  ];
}
